To start using the built-in tests, run <code>npm install</code> first then issue the <code>npm test</code> at the root directory of this project.

WARNING: Do not change any code inside <code>test.js</code>.

1. Take a screenshot of your terminal when you’re done. Attach the image in the mock tech folder. (No need to send in the groupchat.)

2. Create a new repository named s56 and push your mock-tech exam.

3. Add the link in Boodle: WDC028v1.5b-56 | Mock Technical Exam (Concepts and Theory + Function Coding)
